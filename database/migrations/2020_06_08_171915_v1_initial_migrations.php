<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class V1InitialMigrations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscriptions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('unique_id')->default(uniqid());
            $table->string('title');
            $table->text('description');
            $table->integer('no_of_users');
            $table->string('plan_type')->default(PLAN_TYPE_MONTH)->comment="month,year,days";
            $table->string('plan');
            $table->float('amount')->default(0.00);
            $table->integer('total_subscription');
            $table->tinyInteger('status')->default(YES);
            $table->integer('popular_status')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('user_subscriptions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('unique_id')->default(uniqid());
            $table->integer('subscription_id');
            $table->integer('user_id');
            $table->string('payment_id')->default("");
            $table->float('amount')->default(0.00);
            $table->string('payment_mode')->default(COD);
            $table->integer('is_current_subscription')->default(0);
            $table->datetime('expiry_date')->nullable();
            $table->tinyInteger('status')->default(YES);
            $table->tinyInteger('is_cancelled')->default(0);
            $table->text('cancel_reason')->nullable("");
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('meetings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('unique_id')->default(uniqid());
            $table->integer('user_id')->default(0);
            $table->string('username')->default(uniqid());
            $table->string('meeting_name')->default(uniqid());
            $table->time('start_time')->nullable();
            $table->time('end_time')->nullable();
            $table->integer('no_of_users')->default(1);
            $table->tinyInteger('status')->default(YES);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscriptions');
        Schema::dropIfExists('user_subscriptions');
        Schema::dropIfExists('meetings');
    }
}
