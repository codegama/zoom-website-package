<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Meeting extends Model
{	
	protected $hidden = ['deleted_at','id'];

	protected $appends = ['meeting_id','meeting_unique_id','meeting_status'];

    public function getMeetingIdAttribute() {

        return $this->id;
    }

    public function getMeetingUniqueIdAttribute() {

        return $this->unique_id;
    }

    public function getMeetingStatusAttribute() {

        return meeting_status($this->status);
    }

    /**
     * Get the MeetingMember record associated with the Meeting.
     */
    public function meetingMember() {
        
        return $this->hasMany(MeetingMember::class, 'meeting_id');
    }

	/**
     * Get the User that owns the Meeting.
     */
    public function user() {

        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * Scope a query to only include active users.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeCommonResponse($query) {

        return $query->select('meetings.*');
    
    }

    public static function boot() {
       
        parent::boot();

        static::deleting(function ($model) {

            foreach ($model->meetingMember as $key => $meeting_member) {
                
                $meeting_member->delete();                
            }

        });

    }
}
