<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Setting, DB;

use App\Helpers\Helper;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Scope a query to only include active users.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeCommonResponse($query) {

        return $query->select(
            'users.id as user_id',
            'users.username as username',
            'users.name',
            'users.email as email',
            'users.picture as picture',
            'users.description as description',
            'users.mobile as mobile',
            'users.token as token',
            'users.token_expiry as token_expiry',
            'users.social_unique_id as social_unique_id',
            'users.login_by as login_by',
            'users.payment_mode',
            'users.user_card_id',
            'users.status as user_status',
            'users.email_notification_status',
            'users.push_notification_status',
            'users.is_verified as is_email_verified',
            'users.user_type',
            'users.registration_steps',
            'users.created_at',
            'users.updated_at'
            );
    
    }

    /**
     * Scope a query to only include active users.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOtherCommonResponse($query) {

        return $query->select(
            'users.id as user_id',
            'users.username as username',
            'users.name',
            'users.email as email',
            'users.picture as picture',
            'users.mobile as mobile',
            'users.description as description',
            'users.full_address as full_address',
            'users.work',
            'users.school',
            'users.languages',
            'users.social_unique_id as social_unique_id',
            'users.status as user_status',
            'users.is_verified',
            'users.user_type',
            DB::raw("DATE_FORMAT(users.created_at, '%M %Y') as joined") ,
            'users.created_at',
            'users.updated_at'
            );

    }

    /**
     * Get the UserSubscription record associated with the user.
     */
    public function userSubscription() {
        
        return $this->hasMany(UserSubscription::class, 'user_id');
    }    

    /**
     * Get the Meeting record associated with the user.
     */
    public function userMeeting() {
        
        return $this->hasMany(Meeting::class, 'user_id');
    }

    /**
     * Get the UserCard record associated with the user.
     */
    public function userCard() {
        
        return $this->hasMany(UserCard::class, 'user_id');
    }

    public static function boot() {

        parent::boot();

        static::creating(function ($model) {

            $model->attributes['first_name'] = $model->attributes['last_name'] = $model->attributes['name'];

            $model->attributes['is_verified'] = USER_EMAIL_NOT_VERIFIED;

            // if (Setting::get('is_account_email_verification') == YES && envfile('MAIL_USERNAME') && envfile('MAIL_PASSWORD')) { 

                if($model->attributes['login_by'] == 'manual') {

                    $model->generateEmailCode();

                }

            // }

            $model->attributes['status'] = USER_APPROVED;

            $model->attributes['payment_mode'] = COD;

            $model->attributes['username'] = routefreestring($model->attributes['name']);

            $model->attributes['unique_id'] = uniqid();

            $model->attributes['token'] = Helper::generate_token();

            $model->attributes['token_expiry'] = Helper::generate_token_expiry();

            if(in_array($model->attributes['login_by'], ['facebook' , 'google'])) {
                
                $model->attributes['password'] = \Hash::make($model->attributes['social_unique_id']);
            }

        });

        static::created(function($model) {

            $model->attributes['email_notification_status'] = $model->attributes['push_notification_status'] = YES;

            $model->attributes['unique_id'] = "UID"."-".$model->attributes['id']."-".uniqid();

            $model->save();

            /**
             * @todo Update total number of users 
             */
        
        });

        static::updating(function($model) {

            $model->attributes['username'] = routefreestring($model->attributes['name']);

            $model->attributes['first_name'] = $model->attributes['last_name'] = $model->attributes['name'];

        });

        static::deleting(function ($model){

            Helper::delete_file($model->picture , PROFILE_PATH_USER);

            foreach ($model->userSubscription as $key => $user_subscription) {
                
                $user_subscription->delete();
            }

            foreach ($model->userMeeting as $key => $user_meeting) {
                
                $user_meeting->delete();
            }

            foreach ($model->userCard as $key => $user_card) {
                
                $user_card->delete();
            }

        });

    }

    /**
     * Generates Token and Token Expiry
     * 
     * @return bool returns true if successful. false on failure.
     */

    protected function generateEmailCode() {

        $this->attributes['verification_code'] = Helper::generate_email_code();

        $this->attributes['verification_code_expiry'] = Helper::generate_email_expiry();

        // Check Email verification controls and email configurations

        if(Setting::get('is_account_email_verification') == YES && Setting::get('is_email_notification') == YES && Setting::get('is_email_configured') == YES) {

            $this->attributes['is_verified'] = 0;

        } else { 

            $this->attributes['is_verified'] = 1;
        }

        return true;
    
    }

   
}
