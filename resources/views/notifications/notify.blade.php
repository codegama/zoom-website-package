<!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script> -->

@if(Session::has('flash_error'))

    <script type="text/javascript">
    	$(document).ready(function() {
  			
	  		toastr.options = {
		        closeButton: true,
		        "debug": false,
		        "newestOnTop": false,
		        "progressBar": false,
		        "positionClass": "toast-top-right",
		        "preventDuplicates": false,
		        "onclick": null,
		        "showDuration": "3000",
		        "hideDuration": "1000",
		        "timeOut": "5000",
		        "extendedTimeOut": "1000",
		        "showEasing": "swing",
		        "hideEasing": "linear",
		        "showMethod": "fadeIn",
		        "hideMethod": "fadeOut"
		    }

  		  	toastr.error("{{Session::get('flash_error')}}");
		    
		});
		
    </script>

@endif


@if(Session::has('flash_success'))
    <!-- <div class="col-sm-12 col-xs-12 col-md-12 alert alert-success" >
        <button type="button" class="close" data-dismiss="alert">×</button>
        {{Session::get('flash_success')}}
    </div> -->

    <script type="text/javascript">
    	$(document).ready(function() {
  			
	  		toastr.options = {
		        closeButton: true,
		        "debug": false,
		        "newestOnTop": false,
		        "progressBar": false,
		        "positionClass": "toast-top-right",
		        "preventDuplicates": false,
		        "onclick": null,
		        "showDuration": "3000",
		        "hideDuration": "1000",
		        "timeOut": "5000",
		        "extendedTimeOut": "1000",
		        "showEasing": "swing",
		        "hideEasing": "linear",
		        "showMethod": "fadeIn",
		        "hideMethod": "fadeOut"
		    }
            
  		  	toastr.success("{{Session::get('flash_success')}}");
				
		});

    </script>
   
@endif


@if(Session::has('error'))

    <div class="col-sm-12 col-xs-12 col-md-12 alert alert-danger">
        <button type="button" class="close" data-dismiss="alert">×</button>
        {{Session::get('error')}}
    </div>

@endif

@if(Session::has('success'))

    <div class="col-sm-12 col-xs-12 col-md-12 alert alert-success">
        <button type="button" class="close" data-dismiss="alert">×</button>
        {{Session::get('success')}}
    </div>

@endif