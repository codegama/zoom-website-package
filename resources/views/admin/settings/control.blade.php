@extends('layouts.admin') 

@section('title', tr('admin_control'))

@section('breadcrumb')

<li class="breadcrumb-item active" aria-current="page">

    <span>{{ tr('admin_control') }}</span>
</li>

@endsection

@section('content')


<div class="col-lg-12 grid-margin stretch-card">

    <div class="row flex-grow">

        <div class="col-12 grid-margin">

            <div class="card">

                <form class="forms-sample" action="{{ route('admin.settings.save') }}" method="POST" enctype="multipart/form-data" role="form">

                    @csrf

                    <div class="card-header bg-card-header ">

                        <h4 class="">{{tr('admin_control')}}</h4>

                    </div>

                    <div class="card-body">

                        <div class="row">

                            <div class="form-group col-md-6">
                                           
                                <label>{{ tr('is_demo_control_enabled') }}</label>
                                <br>
                                <label>
                                    <input required type="radio" name="is_demo_control_enabled" value="1" class="flat-red" @if(Setting::get('is_demo_control_enabled') == 1) checked @endif>
                                    {{tr('yes')}}
                                </label>

                                <label>
                                    <input required type="radio" name="is_demo_control_enabled" class="flat-red"  value="0" @if(Setting::get('is_demo_control_enabled') == 0) checked @endif>
                                    {{tr('no')}}
                                </label>
                        
                            </div>

                            <div class="form-group col-md-6">
                                           
                                <label>{{ tr('is_account_email_verification') }}</label>
                                <br>
                                <label>
                                    <input required type="radio" name="is_account_email_verification" value="1" class="flat-red" @if(Setting::get('is_account_email_verification') == 1) checked @endif>
                                    {{tr('yes')}}
                                </label>

                                <label>
                                    <input required type="radio" name="is_account_email_verification" class="flat-red"  value="0" @if(Setting::get('is_account_email_verification') == 0) checked @endif>
                                    {{tr('no')}}
                                </label>
                        
                            </div>

                            <div class="form-group col-md-6">
                                           
                                <label>{{ tr('is_email_notification') }}</label>
                                <br>
                                <label>
                                    <input required type="radio" name="is_email_notification" value="1" class="flat-red" @if(Setting::get('is_email_notification') == 1) checked @endif>
                                    {{tr('yes')}}
                                </label>

                                <label>
                                    <input required type="radio" name="is_email_notification" class="flat-red"  value="0" @if(Setting::get('is_email_notification') == 0) checked @endif>
                                    {{tr('no')}}
                                </label>
                        
                            </div>

                            <div class="form-group col-md-6">
                                           
                                <label>{{ tr('is_email_configured') }}</label>
                                <br>
                                <label>
                                    <input required type="radio" name="is_email_configured" value="1" class="flat-red" @if(Setting::get('is_email_configured') == 1) checked @endif>
                                    {{tr('yes')}}
                                </label>

                                <label>
                                    <input required type="radio" name="is_email_configured" class="flat-red"  value="0" @if(Setting::get('is_email_configured') == 0) checked @endif>
                                    {{tr('no')}}
                                </label>
                        
                            </div>

                            <div class="form-group col-md-6">
                                           
                                <label>{{ tr('is_push_notification') }}</label>
                                <br>
                                <label>
                                    <input required type="radio" name="is_push_notification" value="1" class="flat-red" @if(Setting::get('is_push_notification') == 1) checked @endif>
                                    {{tr('yes')}}
                                </label>

                                <label>
                                    <input required type="radio" name="is_push_notification" class="flat-red"  value="0" @if(Setting::get('is_push_notification') == 0) checked @endif>
                                    {{tr('no')}}
                                </label>
                        
                            </div>

                            <div class="form-group col-md-6">
                                           
                                <label>{{ tr('admin_take_count') }}</label>
                                
                                <input type="number" name="admin_take_count" class="form-control" value="{{Setting::get('admin_take_count', 6)}}">
                        
                            </div>

                            <div class="form-group col-md-6">
                                           
                                <label>{{ tr('currency') }}</label>
                                
                                <input type="text" name="currency" class="form-control" value="{{Setting::get('currency', '$')}}">
                        
                            </div>

                            <div class="form-group col-md-6">
                                           
                                <label>{{ tr('currency_code') }}</label>
                                
                                <input type="text" name="currency_code" class="form-control" value="{{Setting::get('currency_code', 'USD')}}">
                        
                            </div>

                        </div>

                        <div class="clearfix"></div>

                        <div class="row">

                            <div class="col-md-12">

                                <hr><h4>Demo Login Details</h4><hr>

                            </div>

                            <div class="form-group col-md-6">
                                           
                                <label>{{ tr('demo_admin_email') }}</label>
                                
                                <input type="text" name="demo_admin_email" class="form-control" value="{{Setting::get('demo_admin_email')}}">
                        
                            </div>

                            <div class="form-group col-md-6">
                                           
                                <label>{{ tr('demo_admin_password') }}</label>
                                
                                <input type="text" name="demo_admin_password" class="form-control" value="{{Setting::get('demo_admin_password')}}">
                        
                            </div>

                            <div class="form-group col-md-6">
                                           
                                <label>{{ tr('demo_user_email') }}</label>
                                
                                <input type="text" name="demo_user_email" class="form-control" value="{{Setting::get('demo_user_email')}}">
                        
                            </div>

                            <div class="form-group col-md-6">
                                           
                                <label>{{ tr('demo_user_password') }}</label>
                                
                                <input type="text" name="demo_user_password" class="form-control" value="{{Setting::get('demo_user_password')}}">
                        
                            </div>
                        
                        </div>

                        <div class="clearfix"></div>

                        <div class="row">

                            <div class="col-md-12">

                                <hr><h4>BBB - Configurations</h4><hr>

                            </div>

                            <div class="form-group col-md-6">

                                <label for="name">BBB Environment<span class="admin-required">*</span> </label>

                                 <div class="form-group clearfix">

                                    <div class="icheck-success d-inline">
                                        <input type="radio" id="BBB_PRODUCTION" name="BBB_ENV"
                                        value="production" @if(Setting::get('BBB_ENV') ==  'production') checked="checked" @endif>
                                        <label for="BBB_PRODUCTION">Production</label>
                                    </div>

                                    <div class="icheck-success d-inline">
                                        <input type="radio" id="BBB_DEBUG" name="BBB_ENV"
                                        value="debug" @if(Setting::get('BBB_ENV') ==  'debug') checked="checked" @endif>
                                        <label for="BBB_DEBUG">Debug</label>
                                    </div>

                                </div>

                            </div>

                            <div class="form-group col-md-6">

                                <label for="name">Recording<span class="admin-required">*</span> </label>

                                 <div class="form-group clearfix">

                                    <div class="icheck-success d-inline">
                                        <input type="radio" id="meeting_recording_on" name="meeting_recording"
                                        value="1" @if(Setting::get('meeting_recording') ==  '1') checked="checked" @endif>
                                        <label for="meeting_recording_on">On</label>
                                    </div>

                                    <div class="icheck-success d-inline">
                                        <input type="radio" id="meeting_recording_off" name="meeting_recording"
                                        value="0" @if(Setting::get('meeting_recording') ==  '0') checked="checked" @endif>
                                        <label for="meeting_recording_off">Off</label>
                                    </div>

                                </div>

                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-12">

                                <h5>Production</h5><hr>
                                
                            </div>
                            <div class="form-group col-md-6">
                                <label for="name">BBB Base URL - Production<span class="admin-required">*</span> </label>
                                <input type="text" class="form-control" id="BBB_SERVER_BASE_URL_PRODUCTION" name="BBB_SERVER_BASE_URL_PRODUCTION" placeholder="{{ tr('name') }}" value="{{ old('BBB_SERVER_BASE_URL_PRODUCTION') ?: Setting::get('BBB_SERVER_BASE_URL_PRODUCTION')}}" required />
                            </div>

                            <div class="form-group col-md-6">
                                <label for="name">BBB Secret - Production<span class="admin-required">*</span> </label>
                                <input type="text" class="form-control" id="BBB_SECRET_PRODUCTION" name="BBB_SECRET_PRODUCTION" placeholder="{{ tr('name') }}" value="{{ old('BBB_SECRET_PRODUCTION') ?: Setting::get('BBB_SECRET_PRODUCTION')}}" required />
                            </div>

                            <div class="form-group col-md-6">
                                <label for="name">BBB RECORD PRESENTATION URL - PRO<span class="admin-required">*</span> </label>
                                <input type="text" class="form-control" id="BBB_RECORD_PRESENTATION_URL_PRO" name="BBB_RECORD_PRESENTATION_URL_PRO" value="{{ old('BBB_RECORD_PRESENTATION_URL_PRO') ?: Setting::get('BBB_RECORD_PRESENTATION_URL_PRO')}}" required />
                            </div>

                            <div class="form-group col-md-6">
                                <label for="name">BBB RECORD VIDEO URL - PRO<span class="admin-required">*</span> </label>
                                <input type="text" class="form-control" id="BBB_RECORD_VIDEO_URL_PRO" name="BBB_RECORD_VIDEO_URL_PRO" value="{{ old('BBB_RECORD_VIDEO_URL_PRO') ?: Setting::get('BBB_RECORD_VIDEO_URL_PRO')}}" required />
                            </div>
                        
                        </div>

                        

                        <div class="row">

                            <div class="col-md-12">
                                <h5>Staging</h5><hr>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="name">BBB Base URL - DEBUG<span class="admin-required">*</span> </label>
                                <input type="text" class="form-control" id="BBB_SERVER_BASE_URL_DEBUG" name="BBB_SERVER_BASE_URL_DEBUG" placeholder="{{ tr('name') }}" value="{{ old('BBB_SERVER_BASE_URL_DEBUG') ?: Setting::get('BBB_SERVER_BASE_URL_DEBUG')}}" required />
                            </div>

                             <div class="form-group col-md-6">
                                <label for="name">BBB Secret - DEBUG<span class="admin-required">*</span> </label>
                                <input type="text" class="form-control" id="BBB_SECRET_DEBUG" name="BBB_SECRET_DEBUG" placeholder="{{ tr('name') }}" value="{{ old('BBB_SECRET_DEBUG') ?: Setting::get('BBB_SECRET_DEBUG')}}" required />
                            </div>

                            <div class="form-group col-md-6">
                                <label for="name">BBB RECORD PRESENTATION URL - DEBUG<span class="admin-required">*</span> </label>
                                <input type="text" class="form-control" id="BBB_RECORD_PRESENTATION_URL_DEBUG" name="BBB_RECORD_PRESENTATION_URL_DEBUG" value="{{ old('BBB_RECORD_PRESENTATION_URL_DEBUG') ?: Setting::get('BBB_RECORD_PRESENTATION_URL_DEBUG')}}" required />
                            </div>

                            <div class="form-group col-md-6">
                                <label for="name">BBB RECORD VIDEO URL - DEBUG<span class="admin-required">*</span> </label>
                                <input type="text" class="form-control" id="BBB_RECORD_VIDEO_URL_DEBUG" name="BBB_RECORD_VIDEO_URL_DEBUG" value="{{ old('BBB_RECORD_VIDEO_URL_DEBUG') ?: Setting::get('BBB_RECORD_VIDEO_URL_DEBUG')}}" required />
                            </div>
                        
                        </div>
                        
                    
                    </div>

                    <div class="card-footer">
                        <button type="submit" class="btn btn-success mr-2">{{ tr('submit') }}</button>
                    </div>

                </form>

            </div>

        </div>
    
    </div>

</div>
@endsection


@section('scripts')


@endsection