@extends('layouts.admin')

@section('page_header',tr('meetings'))

@section('breadcrumbs')

<li class="breadcrumb-item active"><a href="javascript:void(0)"></a>{{tr('meetings')}}</li>

@endsection

@section('content')

<div class="card">

    <div class="card-header bg-info">

        <h4 class="m-b-0 text-white">{{ $title }}

            <div class="dropdown btn btn-secondary pull-right admin_action">
                <button class="m-b-0 text-white" type="button" id="dropdownMenuOutlineButton1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    {{tr('admin_bulk_action')}}
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuOutlineButton1">
                    @if(Setting::get('is_demo_control_enabled') == NO)
                    <a class="action_list dropdown-item" id="bulk_delete" href="#">
                        {{tr('delete')}}
                    </a>    
                    @else
                    <a class="dropdown-item" href="javascript:;">{{tr('bulk_delete')}}</a>                           
                    @endif
                </div>
            </div>
        </h4>

    </div>

	<div class="card-body">

        <div class="bulk_action">

            <form  action="{{route('admin.meetings.bulk_action.delete')}}" id="admin_meetings" method="POST" role="search">

                @csrf

                <input type="hidden" name="action_name" id="action" value="">

                <input type="hidden" name="selected_meetings" id="selected_ids" value="">

                <input type="hidden" name="page_id" id="page_id" value="{{ (request()->page) ? request()->page : '1' }}">

            </form>
        </div>

        @include('admin.meetings._search')

		<div class="table-responsive">

            @if(count($meetings) > 0)

                <table id="actionDataTable" class="table data-table">

                    <thead>
                        <tr>
                            <th>
                                <input id="check_all" type="checkbox">
                            </th>
                            <th>{{tr('s_no')}}</th>
							<th>{{tr('username')}}</th>
							<th>{{tr('meeting_name')}}</th>
							<th>{{tr('start_time')}}</th>
							<th>{{tr('end_time')}}</th>
							<th>{{tr('no_of_users')}}</th>
                            <th>{{tr('status')}}</th>
                            <th>{{tr('action')}}</th>
                        </tr>
                    </thead>

                    <tbody>

                    	@foreach($meetings as $i => $meeting_details)
                           
                            <tr>
                                <td><input type="checkbox" name="row_check" class="faChkRnd" id="{{$meeting_details->id}}" value="{{$meeting_details->id}}"></td>
                           
                                <td>{{$i+$meetings->firstItem()}}</td>

                                <td>
                                    <a href="{{ route('admin.users.view', ['user_id' => $meeting_details->user_id]) }}">{{($meeting_details->user) ? $meeting_details->user->name : tr('user_not_available')}}
                                    </a>
                                </td>

                                <td>
                                    <a href="{{ route('admin.meetings.view', ['meeting_id' => $meeting_details->id]) }}">{{$meeting_details->meeting_name ?? tr('meeting_not_available')}}
                                    </a>
                                </td>

                                <td>{{ common_date($meeting_details->start_time, Auth::guard('admin')->user()->timezone,'H:i:s') }}
                                </td> 

                                <td>
                                     @if($meeting_details->end_time != '00:00:00')
                                     {{ common_date($meeting_details->end_time, Auth::guard('admin')->user()->timezone,'H:i:s') }}
                                     @else
                                     {{ $meeting_details->end_time }}
                                     @endif
                                </td>

                                <td> <a href="{{ route('admin.meetings.view', ['meeting_id' => $meeting_details->id, 'type' => 'view_members']) }}"> {{$meeting_details->meetingMember->count() ?? 0}} </a> </td>

                                <td>
						      		@if($meeting_details->status == MEETING_INITIATED)
									   <span class="badge badge-success">{{tr('meeting_initiated')}}</span>
									@elseif($meeting_details->status == MEETING_STARTED)
									   <span class="badge badge-primary">{{tr('meeting_started')}}</span>
                                    @else
                                       <span class="badge badge-info">{{tr('meeting_completed')}}</span>
									@endif
						      	</td>

                                <td>
                                    
                                    <div class="dropdown">

                                        <button class="btn btn-outline-primary  dropdown-toggle btn-sm" type="button" id="dropdownMenuOutlineButton1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        {{tr('action')}}
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuOutlineButton1">
                                          
                                            <a class="dropdown-item" href="{{ route('admin.meetings.view', ['meeting_id' => $meeting_details->id]) }}">
                                                {{tr('view')}}
                                            </a>
                                            
                                            @if(Setting::get('is_demo_control_enabled') == NO)
                                                
                                                <a class="dropdown-item" href="{{route('admin.meetings.delete', ['meeting_id' => $meeting_details->id,'page'=>$page])}}" 
                                                onclick="return confirm(&quot;{{tr('meeting_delete_confirmation', $meeting_details->meeting_name)}}&quot;);">
                                                    {{tr('delete')}}
                                                </a>
                                            @else

                                                <a class="dropdown-item" href="javascript:;">{{tr('delete')}}</a>                           
                                            @endif
                                            
                                            @if($meeting_details->status == MEETING_INITIATED || $meeting_details->status == MEETING_STARTED)
                                            <div class="dropdown-divider"></div>

                                                <a class="dropdown-item" href="{{ route('admin.meetings.end', ['meeting_id' => $meeting_details->id]) }}" onclick="return confirm(&quot;{{tr('meeting_end_confirmation', $meeting_details->meeting_name)}}&quot;);">
                                                {{tr('end_meeting')}}
                                                </a>
                                            @endif
                                        </div>

                                    </div>

                                </td>
                             
                            </tr>
                        @endforeach
                       
                    </tbody>
                    
                </table>

                <div class="pull-right">{{ $meetings->appends(request()->input())->links() }}</div>

            @else

                <h3 class="no-result text-center pt-2">{{ tr('no_meetings_found') }}</h3>
                
            @endif

        </div>
        
	</div>
	
</div>

@endsection

@section('scripts')
    
@if(Session::has('bulk_action'))
<script type="text/javascript">
    $(document).ready(function(){
        localStorage.clear();
    });
</script>
@endif

<script type="text/javascript">

    $(document).ready(function(){
        get_values();

        $('.action_list').click(function(){
            var selected_action = $(this).attr('id');
            if(selected_action != undefined){
                $('#action').val(selected_action);
                if($("#selected_ids").val() != ""){
                    if(selected_action == 'bulk_delete'){
                        var message = "<?php echo tr('admin_meetings_delete_confirmation') ?>";
                    }
                    var confirm_action = confirm(message);

                    if (confirm_action == true) {
                      $( "#admin_meetings" ).submit();
                    }
                    // 
                }else{
                    alert('Please select the check box');
                }
            }
        });
    // single check
    var page = $('#page_id').val();
    $(':checkbox[name=row_check]').on('change', function() {
        var checked_ids = $(':checkbox[name=row_check]:checked').map(function() {
            return this.id;
        })
        .get();

        localStorage.setItem("meeting_checked_items"+page, JSON.stringify(checked_ids));

        get_values();

    });
    // select all checkbox
    $("#check_all").on("click", function () {
        if ($("input:checkbox").prop("checked")) {
            $("input:checkbox[name='row_check']").prop("checked", true);
            var checked_ids = $(':checkbox[name=row_check]:checked').map(function() {
                return this.id;
            })
            .get();

            localStorage.setItem("meeting_checked_items"+page, JSON.stringify(checked_ids));
            get_values();
        } else {
            $("input:checkbox[name='row_check']").prop("checked", false);
            localStorage.removeItem("meeting_checked_items"+page);
            get_values();
        }

    });

    function get_values(){
        var pageKeys = Object.keys(localStorage).filter(key => key.indexOf('meeting_checked_items') === 0);
        var values = Array.prototype.concat.apply([], pageKeys.map(key => JSON.parse(localStorage[key])));

        if(values){
            $('#selected_ids').val(values);
        }

        for (var i=0; i<values.length; i++) {
            $('#' + values[i] ).prop("checked", true);
        }

}

});
</script>
@endsection