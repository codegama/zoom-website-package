@extends('layouts.admin')

@section('page_header',tr('subscription_payments'))

@section('breadcrumbs')

<li class="breadcrumb-item active"><a href="javascript:void(0)"></a>{{tr('subscription_payments')}}</li>
@endsection

@section('content')

<div class="card">

    <div class="card-header bg-info">

        <h4 class="m-b-0 text-white">{{tr('view_subscription_payments')}}</h4>

    </div>

    <div class="card-body">

        <div class="row">


            <div class="col-8">

                <!-- Card content -->
                <div class="card-body">

                    <div class="template-demo">

                        <table class="table mb-0">

                            <thead>

                            </thead>

                            <tbody>

                                <tr>
                                    <td class="pl-0"><b>{{ tr('name') }}</b></td>
                                    <td class="pr-0 text-right">
                                        <div>
                                            <a href="{{route('admin.users.view',['user_id'=>$user_subscription_details->user_id])}}">
                                                <span class="text-green pull-right">{{$user_subscription_details->user_details->username??''}}
                                                </span></a></div>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="pl-0"><b>{{ tr('email') }}</b></td>
                                    <td class="pr-0 text-right">
                                        <div>{{$user_subscription_details->user_details->email??''}}</div>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="pl-0"><b>{{tr('subscriptions')}}</b></td>

                                    <td class="pr-0 text-right">
                                        <a href="{{route('admin.subscriptions.view',['subscription_id'=>$user_subscription_details->subscription_id])}}">

                                            <span class="text-green pull-right">
                                                {{$user_subscription_details->subscription_details->title ?? ''}}
                                            </span>
                                    </td>
                                    </a>
                                </tr>


                                <tr>
                                    <td class="pl-0"><b>{{ tr('no_of_users') }}</b></td>
                                    <td class="pr-0 text-right">
                                        <div>
                                            <span class="text-green pull-right">{{$user_subscription_details->no_of_users}}
                                            </span></div>
                                    </td>
                                </tr>


                                <tr>
                                    <td class="pl-0"><b>{{ tr('no_of_hours') }}</b></td>
                                    <td class="pr-0 text-right">
                                        <div>
                                            <span class="text-green pull-right">{{$user_subscription_details->no_of_hrs}}
                                            </span></div>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="pl-0"><b>{{tr('payment_id')}}</b></td>
                                    <td class="pr-0 text-right">
                                        <span class="pull-right">
                                            {{$user_subscription_details->payment_id}}
                                        </span>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="pl-0"><b>{{tr('payment_mode')}}</b></td>
                                    <td class="pr-0 text-right">
                                        <span class="pull-right">
                                            {{$user_subscription_details->payment_mode ?? 'free-plan'}}
                                        </span>
                                        </a>
                                    </td>
                                </tr>


                                <tr>
                                    <td class="pl-0"><b> {{tr('paid_amount')}}</td>
                                    <td class="pr-0 text-right">
                                        <span class="pull-right">
                                            {{formatted_amount($user_subscription_details->amount ?? "0.00")}}
                                        </span>
                                    </td>
                                </tr>

                            </tbody>

                        </table>

                    </div>

                </div>

                <div class="grid-margin stretch-card"></div>



            </div>









        </div>

    </div>

</div>

@endsection