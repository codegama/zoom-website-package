<header class="main-header" id="header">

    <nav class="navbar navbar-expand-lg navbar-light" id="navbar">
        
        <button id="sidebar-toggler" class="sidebar-toggle">
            <span class="sr-only">Toggle navigation</span>
        </button>


        <div class="navbar-right ">

           
           
            <ul class="nav navbar-nav">

                 <li class="nav-item d-none d-lg-block">
                <a class="btn btn-primary btn-fw" id="visit-website" href="{{Setting::get('frontend_url')}}" target="_blank">
                    Visit Website
                </a>
            </li>
              
                <li class="dropdown user-menu">

                    <button class="dropdown-toggle nav-link" data-toggle="dropdown">
                        <img src="{{Auth::guard('admin')->user()->picture ?? '-'}}" class="user-image rounded-circle header-profile-image" />
                        <span class="d-none d-lg-inline-block">{{Auth::guard('admin')->user()->name ?? '-'}}</span>
                    </button>

                    <ul class="dropdown-menu dropdown-menu-right">
                        <li>
                            <a class="dropdown-link-item" href="{{route('admin.profile')}}">
                                <i class="mdi mdi-account-outline"></i>
                                <span class="nav-text">{{tr('my_profile')}}</span>
                            </a>
                        </li>

                        <li class="dropdown-footer">
                            <a class="dropdown-link-item"  data-toggle="modal" data-target="#logoutModel"> <i class="mdi mdi-logout pull-left"></i> {{tr('logout')}}</a>
                        </li>
                    </ul>

                </li>

            </ul>

        </div>

    </nav>

</header>